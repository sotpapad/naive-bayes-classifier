% Naive Bayes Classifier prediction algorithm

% Input:
%       X_categorical: JxM categorical variables matrix; rows are samples
%                       and columns are variables
%       X_continuous: JxM continuous variables matrix; same structure as
%                       X_categorical
%       model: all params NBC requires to classify new samples - computed
%               using the train_NBC function
% Output:
%       predictions: Jx1 vector; predicted class for each of the input
%                       samples

function predictions = predict_NBC(model, X_categorical, X_continuous)

    % get the X matrices dimensions
    J_cat = size(X_categorical,1);      % rows
    M_cat = size(X_categorical,2);      % cols
    J_cont = size(X_continuous,1);      % rows
    M_cont = size(X_continuous,2);      % cols
    
    % examine what type of input variables exist in the dataset, in order
    % to know the correct model and process of prediction
    
    % make prediction for categorical input
    if ( ~isempty(X_categorical) && isempty(X_continuous) )
        % initialize predictions vector
        predictions = zeros(J_cat,1);
        
        % retrieve model parameters
        LM = model{1};                  % {1,M}{categories of var,classes}
        PR_cat = model{2};              % (1,classes)
        max_class = model{3};           % double
        
        for r=1:J_cat                   % for each sample
            sample = X_categorical(r,:);

            % initialize conditional probabliities vector of the MAP estimates
            % (rows as many as sample length, columns as many as classes)
            cond_probs = zeros(length(sample),max_class+1); % length(sample)==M_cat

            for c=1:M_cat
                % find likelihood of a certain category and of all classes
                cond_probs(c,:) = LM{c}(sample(c)+1,:);
            end

            % the prediction is the class associated to the maximum among
            % the different products that represent the nominators
            % (if there are more than 1 max, then choose the first)
            [~, i] = max( PR_cat .* prod(cond_probs) );
            predictions(r) = i-1;
        end

    % make prediction for continuous input variables
    elseif ( ~isempty(X_continuous) && isempty(X_categorical) )
        % initialize predictions vector
        predictions = zeros(J_cont,1);
        
        % retrieve model parameters
        MEAN = model{1};        % {1,M}{1,classes}
        SIGMA = model{2};       % {1,M}{1,classes}
        PR_cont = model{3};     % (1,classes)
        max_class = model{4};   % double
        
        for r=1:J_cont                   % for each sample
            sample = X_continuous(r,:);
            
            % initialize conditional probabliities vector of the MLE estimates
            % (rows as many as sample length, columns as many as classes)
            gauss_probs = zeros(length(sample),max_class+1);

            for c=1:M_cont
                % find likelihood of a certain variable value and of all
                % classes
                gauss_probs(c,:) = 1./(sqrt(2*pi).*SIGMA{c}) .*...
                                    exp( -(sample(c)-MEAN{c}).^2 ./ (2.*(SIGMA{c}.^2)) );
            end
            
            % the prediction is the class associated to the maximum among
            % the different products that represent the nominators
            % (if there are more than 1 max, then choose the first)
            [~, i] = max( PR_cont .* prod(gauss_probs) );
            predictions(r) = i-1;
        end

    % make prediction for mixed input variables    
    elseif ( ~isempty(X_categorical) && ~isempty(X_continuous) )
        % initialize predictions vector
        predictions = zeros(J_cont,1);
        
        % retrieve model parameters
        LM = model{1};          % {1,M}{categories of var,classes}
        PR_cat = model{2};      % (1,classes)
        max_class = model{3};   % double
        MEAN = model{4};        % {1,M}{1,classes}
        SIGMA = model{5};       % {1,M}{1,classes}
        %PR_cont = model{6};     % (1,classes)
    
        % now repeat the same steps, except for the probablities computation
        % and hence the class estimation

        for r=1:J_cat                   % for each sample
            % first for the categorical inputs:
            sample_cat = X_categorical(r,:);

            % initialize conditional probabliities vector of the MAP estimates
            % (rows as many as sample length, columns as many as classes)
            cond_probs = zeros(M_cat,max_class+1);

            for c=1:M_cat
                % find likelihood of a certain category and of all classes
                cond_probs(c,:) = LM{c}(sample_cat(c)+1,:);
            end
        
            % then for the continuous inputs:
            sample_cont = X_continuous(r,:);
            
            % initialize conditional probabliities vector of the MLE estimates
            % (rows as many as sample length, columns as many as classes)
            gauss_probs = zeros(M_cont,max_class+1);

            for c=1:M_cont
                % find likelihood of a certain variable value and of all
                % classes
                gauss_probs(c,:) = 1./(sqrt(2*pi).*SIGMA{c}) .*...
                                    exp( -(sample_cont(c)-MEAN{c}).^2 ./ (2.*(SIGMA{c}.^2)) );
            end
            
            % the prediction is the class associated to the maximum among
            % the different products, that represent the nominators
            % (if there are more than 1 max, then choose the first)
            % because smoothing may have been applied to the categorical
            % variables input during training, choose the priors that
            % correspond to them
            [~, i] = max( PR_cat .* prod(cond_probs) .* prod(gauss_probs) );
            predictions(r) = i-1;
        end

    end
end
