Naive Bayes Classification algorithms

The algorithms can handel both categorical (discrete)-valued and 
continuous-valued inputs

train_NBC: training algorithm using a training subset of the data

predict_NBC: predictions algorithm using a test subset of the data