% Naive Bayes Classifier training algorithm

% Input:
%       X_categorical: JxM categorical variables matrix; rows are samples
%                       and columns are variables
%       X_continuous: JxM continuous variables matrix; same structure as
%                       X_categorical
%       Y: Jx1 vector; the class of each sample in the training set
%       L: scalar; Laplace smoothing parameter of MAP estimates equation
%       D_categorical: 1xM vector; D(i) contains the number of possible
%                       values for categorical variable xi
% Output:
%       model: all params NBC requires to classify new samples

function model = train_NBC(X_categorical, X_continuous, Y, L, D_categorical)

    % make sure L does not take on negative values
    if L < 0
        error = "L not equal to or larger than 0.";
        baseException = MException(error);
        throw(baseException);
    end
    
    % make sure that the classes take labels starting from 0 and if they
    % don't, force them; then find the max class label
    min_class = min(Y);
    Y = Y - min_class;
    max_class = max(Y);
    
    %% ----- handle X_categorical -----
    
    if ~isempty(X_categorical)
        % get the X matrix dimensions and the number of categories, then
        % make sure that they take labels starting from 0
        %J_cat = size(X_categorical,1);      % rows
        M_Cat = size(X_categorical,2);      % cols
        min_cat = min(X_categorical);
        X_categorical = X_categorical - min_cat;

        % initialize the frequency and likelihood matrices (1 for each variable)
        FM = cell(1,M_Cat);         % cell with all frequency matrices
        LM = cell(1,M_Cat);         % cell with all likelihood matrices

        % initialize the prior probabilities
        PR_cat = zeros(1,max_class+1);

        for c=1:M_Cat                            % for each col of X
            XY = [X_categorical(:,c) Y];         % get corresponding class
            ctgrs = D_categorical(c);            % number of unique xi
            FM{c} = zeros(ctgrs,max_class+1);    % (unique xi, unique yj)

            % assign the appropriate values to FM
            for i=0:ctgrs-1                              % for each unique xi
                for j=0:max_class                        % for each unique yj
                    rows = find(XY(:,1)==i & XY(:,2)==j);
                    freq = length(rows);                 % no of satisfied cond probabilies
                    FM{c}(i+1,j+1) = freq;
                end
            end

            % perform smoothing
            FM{c} = FM{c} + L;

            % covert FM to LM (divide FM by respective column sum)
            LM{c} = FM{c} ./ sum(FM{c});

        end
        
        % get the prior propabilities from one of the FM
        % (get each column sum and divide by total number)
        PR_cat = sum(FM{1}) ./ sum( sum(FM{1}) );
        
        model_cat = {LM, PR_cat, max_class};
    end
    
    %% ----- handle X_continuous -----
    
    if ~isempty(X_continuous)
        % get the X matrix dimensions
        %J_cont = size(X_continuous,1);      % rows
        M_cont = size(X_continuous,2);      % cols

        % initialize the prior probabilities, mean and std
        PR_cont = zeros(1,max_class+1);
        MEAN = cell(1,M_cont);
        SIGMA = cell(1,M_cont);

        % iterate through each variable and find mean and std given a
        % certain class
        for c=1:M_cont                          % for each col of X
            XY = [X_continuous(:,c) Y];         % get corresponding class

            % initialize values for each class (as many as classes exist)
            MEAN{c} = zeros(1,max_class+1);
            SIGMA{c} = zeros(1,max_class+1);

            for j=0:max_class                    % for each unique yj
                rows = find(XY(:,2)==j);
                MEAN{c}(j+1)= mean( XY(rows) );     % compute mean
                SIGMA{c}(j+1) = std( XY(rows), 1);  % compute std
            end

        end
        
        % iterate through Y and find number of each class (since no
        % smoothing is applied to alter the frequences of the classes)
        for j=0:max_class                    % for each unique yj
            class = find(Y==j);
            PR_cont(j+1) = length(class);    % assign frequency
        end
        PR_cont = PR_cont ./ sum(PR_cont);   % divide by total number
        
        model_cont = {MEAN, SIGMA, PR_cont, max_class};
    end
    
    %% return the correct model
    if ( ~isempty(X_categorical) && isempty(X_continuous) )
        model = model_cat;
    elseif ( ~isempty(X_continuous) && isempty(X_categorical) )
        model = model_cont;
    elseif ( ~isempty(X_categorical) && ~isempty(X_continuous) )
        model = [model_cat, model_cont];
    end
end
